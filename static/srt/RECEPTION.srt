1
00:00:31,375 --> 00:00:35,250
_a_partir_daqui

2
00:00:35,250 --> 00:00:40,750
_você é viajante
_na Teia de Medeia

3
00:00:43,583 --> 00:00:50,666
_Medeia_
traída/expatriada/hacker/cigana

4
00:00:53,916 --> 00:00:59,666
_é preciso entender
_que algumas cenas
_poderão conter imagens fortes

5
00:00:59,666 --> 00:01:04,541
_como num filme de terror

6
00:01:07,875 --> 00:01:12,833
_para percorrer a teia
_clique nos ícones

7
00:01:12,833 --> 00:01:18,833
_novelos_infinitos_cartas_facas

8
00:01:22,166 --> 00:01:27,125
_alguns conteúdos
_precisam ser destrancados

9
00:01:27,125 --> 00:01:32,083
_você descobrirá
_como destrancar

10
00:01:32,083 --> 00:01:37,625
_use o mapa para
_visualizar os caminhos possíveis

11
00:01:37,625 --> 00:01:42,708
_e saber em que setor
_você se encontra

12
00:01:42,708 --> 00:01:51,833
 _você tem 72hs
_para explorar a teia

13
00:01:54,166 --> 00:02:01,875
 _para mais informações
_sobre o projeto
_clique no novelo

14
00:02:05,291 --> 00:02:14,791
 _boa viagem_

