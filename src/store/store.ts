import { writable } from 'svelte/store'
import { browser } from '$app/environment'

export const startDate = new Date("2023-7-6")
export const endDate = new Date("2023-7-10")

function lockedView() {
	const { subscribe, set } = writable(browser && localStorage && localStorage.getItem('freeAccess') === 'true' ? false : true);

	return {
		subscribe,
		unlock: () => {
      set(false)
      if(browser && localStorage) window.localStorage.setItem('freeAccess', 'true')
    }
  }
}

function globalMuted() {
	const { subscribe, set } = writable(true);

	return {
		subscribe,
		mute: () => set(true),
		unmute: () => set(false),
    set: (value: boolean) => set(value)
	}
}

function textLanguage() {
	const { subscribe, update, set } = writable(browser && localStorage && localStorage.getItem('lang') === 'en' ? 'en' : 'pt');

	return {
		subscribe,
		change: () => {
      update(lang => {
        const newLang = lang === 'pt' ? 'en' : 'pt';
        if(browser && localStorage) localStorage.setItem('lang', newLang)
        return newLang;
      })
    },
    en: () => {
      set('en')
      if(browser && localStorage) localStorage.setItem('lang', 'en')
    }
	}
}

function mainMenu() {
	const { subscribe, set } = writable(false);

	return {
		subscribe,
		open: () => set(true),
		close: () => set(false)
	}
}

function is18() {
	const { subscribe, set } = writable(browser && localStorage && localStorage.getItem('minimalAge') === 'true' ? true : false)

	return {
		subscribe,
		confirm: () => {
      set(true)
      if(browser && localStorage) window.localStorage.setItem('minimalAge', 'true')
    }
	}
}

function sensibleContent() {
	const { subscribe, set } = writable(false)

	return {
		subscribe,
		confirm: () => { set(true) }
	}
}

function markViewed() {
	const { subscribe, update } = writable({
    teia: browser && localStorage && localStorage.getItem('teia') === 'true' ? true : false,
    escrita: browser && localStorage && localStorage.getItem('escrita') === 'true' ? true : false,
    eremita: browser && localStorage && localStorage.getItem('eremita') === 'true' ? true : false,
    olhos: browser && localStorage && localStorage.getItem('olhos') === 'true' ? true : false,
    filhos: browser && localStorage && localStorage.getItem('filhos') === 'true' ? true : false,
    jasao: browser && localStorage && localStorage.getItem('jasao') === 'true' ? true : false,
    sangue: browser && localStorage && localStorage.getItem('sangue') === 'true' ? true : false,
    taro: browser && localStorage && localStorage.getItem('taro') === 'true' ? true : false
  })

	return {
		subscribe,
		setViewed: (page: string) => {
      update( n => {
        if(page in n) {
          n[page] = true
          if(browser && localStorage) window.localStorage.setItem(page, 'true')
        }
        return n
      })
    }
	}
}

export const muted = globalMuted()
export const isMenuOpen = mainMenu()
export const hasMinimalAge = is18()
export const isLocked = lockedView()
export const pageViewed = markViewed()
export const allowSensible = sensibleContent()
export const language = textLanguage()
