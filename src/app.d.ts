// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
// and what to do when importing types
declare namespace App {
	// interface Locals {}
	// interface PageData {}
	// interface Error {}
	// interface Platform {}
}

interface currentTime {
  duration: number
  percent: number
  seconds: number
}

interface pageFormat {
  name: string,
  type: "pages" | "pics" | "videos" | "external",
  videoId: string,
  videoKey: string
  links: linksInterface[]
}

interface videoFormat {
  "type": "video" | "gallery" ,
  "title": string,
  "options": {
    "videoId": string,
    "videoKey": string
  }
}

interface galleryFormat {
  "type": "video" | "gallery" ,
  "title": string,
  "options": {
    "images": string[]
  }
}

interface subtitle {
  index: number
  start: string
  end: string
  text: string
}

interface linksInterface {
  "link": string,
  "type": string,
  "locked"?: string
}

interface markerInterface {timecode: string, text: string}