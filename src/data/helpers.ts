export const timecodeToSeconds = (timecode: string) => {
  const timeArray = timecode.split(":")
  const minutes = parseInt(timeArray[1]) * 60
  const seconds = parseInt(timeArray[2].split(",")[0])
  const frames  = parseInt(timeArray[2].split(",")[1])/1000
  return minutes + seconds + frames
}