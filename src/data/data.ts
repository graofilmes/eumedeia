import pages from './pages.json'
import medias from './medias.json'
import keys from './keys.json'

export default {
  pages,
  medias,
  keys
}