/** @type {import('@sveltejs/kit').ParamMatcher} */

import pageInfos from "../data/pages.json"

export function match(param: string) {
  return pageInfos.hasOwnProperty(param)
}