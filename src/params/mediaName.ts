/** @type {import('@sveltejs/kit').ParamMatcher} */

import videoInfos from "../data/medias.json"

export function match(param: string) {
  return videoInfos.hasOwnProperty(param)
}