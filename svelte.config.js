import staticAdapter from '@sveltejs/adapter-static'
import preprocess from 'svelte-preprocess'
import autoprefixer from 'autoprefixer'

/** @type {import('@sveltejs/kit').Config} */
const config = {
  preprocess: preprocess({
    postcss: {
      plugins: [autoprefixer]
    },
  }),

	kit: {
    adapter: staticAdapter({
      pages: 'build',
      assets: 'build',
      fallback: 'index.html',
      precompress: false
    }),
    alias: {
      $assets: "./src/assets",
      $data: "./src/data",
      $store: "./src/store"
    }
	}
}

export default config;
