/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js,svelte,ts}', './static/index.html'],
  theme: {
    extend: {
      screens: {
        'portrait': { 'raw': '(orientation: portrait)' },
        'touch': {'raw': '(pointer: coarse)'}
      }
    }
  },
  plugins: []
};